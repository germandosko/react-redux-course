// @flow
import { camelCase, isObjectLike, reduce, isString } from 'lodash'
import type { User } from 'src/redux/modules/user/types'

export type BackendProps =
  | User
  | string

// Standardize camelCasing convention
const reducer = (el: Object) =>
  reduce(el, (acc, value, key) =>
    ({ ...acc, [camelCase(key)]: value }), {})

export default (elements: BackendProps) => {

  if (Array.isArray(elements)) {

    return elements.map(el => reducer(el))

  }
  if (isObjectLike(elements)) {

    if (!isString(elements)) reducer(elements)

  }
  return elements

}
