// @flow
import { get } from 'src/helpers/api'
import mapFetchProps from 'src/helpers/etc/mapFetchProps'
import env from 'config/env'
import {
  getUsersMock,
} from './mocks'

const { USE_MOCK_API } = env

const getUsers = () => (
  USE_MOCK_API
    ? getUsersMock()
    : get(`/users`)
      .then(({ data }) =>
        mapFetchProps(data)
      )
)

export default getUsers
