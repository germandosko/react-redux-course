// @flow
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { cleanUserList, getUsers } from 'src/redux/modules/user/actions'
import type { User } from 'src/redux/modules/user/types'
import type { RootReducerState } from 'src/redux/modules'
import Users from './Users'


type StateProps = {
  usersList: Array<User>,
}

const mapStateToProps = (state: RootReducerState): StateProps => ({
  usersList: state.user.list,
})

type DispatchProps = {
  cleanUserList: Function,
  getUsers: Function,
}

const mapDispatchToProps = (dispatch: any): DispatchProps =>
  bindActionCreators({
    cleanUserList,
    getUsers,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Users)
export type ReduxProps = StateProps & DispatchProps
