// @flow
import React, { PureComponent } from 'react'
import css from './Users.style.css'

import type { ReduxProps } from './'


type Props = ReduxProps


class Users extends PureComponent<Props> {

  componentDidMount () {

    this.props.getUsers()

  }

  // TODO: watch for resolution of
  // https://github.com/yannickcr/eslint-plugin-react/issues/1376
  props: Props

  render () {

    const { usersList, cleanUserList } = this.props

    const finalUserList = usersList.map(user =>
      <li key={user.id}>{user.name}</li>
    )

    return (
      <div className={css.todos}>
        <div className={css.content}>
          Users List
          <ul>
            {finalUserList}
          </ul>
          <button
            onClick={cleanUserList}
          >
            Clean User List
          </button>
        </div>
      </div>
    )

  }

}


export default Users
