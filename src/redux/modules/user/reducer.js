// @flow
// Non-shallow reducer state example needs Immutable
// Async actions need redux-observable epics
import { CLEAN_USER_LIST, GET_USERS } from './consts'
import type { UserState } from './types'
import initialState from './initialState'

// REDUCER
function reducer (state: UserState = initialState, action: GlobalFSA<any>) {

  switch (action.type) {

    case CLEAN_USER_LIST:
      return {
        ...state,
        list: [],
      }

    case `${GET_USERS}_PENDING`:
      return {
        ...state,
        isFeching: true,
      }

    case `${GET_USERS}_FULFILLED`:
      return {
        ...state,
        list: action.payload,
        isFeching: false,
      }

    case `${GET_USERS}_REJECTED`:
      return {
        ...state,
        errorMessage: action.payload,
        isFeching: false,
      }

    default:
      return state

  }

}

// EPICS
// variable$ notation indicates an event stream
// https://redux-observable.js.org/docs/basics/Epics.html

export default reducer
