// @flow
import getUserApi from 'src/helpers/api/user'
import { CLEAN_USER_LIST, GET_USERS } from './consts'

// Use redux-promise-middleware
// https://github.com/pburtchaell/redux-promise-middleware/blob/master/docs/guides/chaining-actions.md
// Which, in turn, uses Flux Standard Action (FSA) notation
// https://github.com/acdlite/flux-standard-action
export const cleanUserList = () => ({
  type: CLEAN_USER_LIST,
})

export const getUsers = () => ({
  type: GET_USERS,
  payload: getUserApi(),
})
