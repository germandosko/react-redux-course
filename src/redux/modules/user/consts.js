// @flow
export const CLEAN_USER_LIST = 'my-app/user/CLEAN_USER_LIST'
export const GET_USERS = 'my-app/user/GET_USERS'
