// @flow
import type { UserState } from './types'

const initialState: UserState = {
  list: [],
  isFetching: false,
  errorMessage: '',
}

export default initialState
